package com.sg.mc.bb;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class BlockBlockerControl extends JavaPlugin implements Listener {

	
	
	@Override
	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(this, this);
		
		
		
		
		super.onEnable();
	}


	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void intercept(BlockPlaceEvent e) {
		
		if(e.getBlock().getType().equals(Material.COMMAND) && !e.getPlayer().isPermissionSet("bb.commandblock")) {
			e.getPlayer().sendMessage("�4You don't have permission to use Command Blocks on this server. Sorry.");
			e.setCancelled(true);
		} else if(e.getBlock().getType().equals(Material.COMMAND)) {
			e.getPlayer().sendMessage("�2You DO have permission, I suppose...");
		}
		
		
	}
}
